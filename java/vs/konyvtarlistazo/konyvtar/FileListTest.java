package vs.konyvtarlistazo.konyvtar;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class FileListTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testPath() {
		String testPath = "c:\\work";
		File folder = new File(testPath);
		Assert.assertEquals(folder.getAbsolutePath(), testPath);
	}

	@Test
	public void testFoldersExist() {
		String testPath = "c:\\work";
		File folder = new File(testPath);
		File[] listOfFiles = folder.listFiles();
		Assert.assertNotNull(listOfFiles);

	}

	@Test
	public void testFolderContent() {
		String fileContent = "test";
		String path = "c:\\test.txt";
		FileKiir fk = new FileKiir(path, fileContent);
		fk.stringKiir();
		File folder = new File("c:\\");
		File[] listOfFiles = folder.listFiles();
		boolean fileExist = false;
		for (File file : listOfFiles) {
			if (file.getName().equals("test.txt")) {
				fileExist = true;
			}
		}
		if (fileExist != true) {
			fail("Test file name can't readable.");
		}

	}

}
