package vs.konyvtarlistazo.konyvtar;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;



public class DbKiirTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dir_nyt", "root", "1234");
		DbKiir dk = new DbKiir(conn);
		dk.write("testText");

	}

	@Test
	public void test() {
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dir_nyt", "root", "1234");
			PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM directory");
			ResultSet result = preparedStatement.executeQuery();
			int dirId = 0;
			String dir = "";
			while (result.next()) {
				dirId = result.getInt("dir_id");
				dir = result.getString("dir");
			}
			Assert.assertEquals(dir, "testText");

			preparedStatement = conn.prepareStatement("DELETE FROM directory WHERE dir_id=?");
			preparedStatement.setInt(1, dirId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
