package vs.konyvtarlistazo.konyvtar;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class FileKiirTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String fileContent = "test";
		String path = "c:\\test.txt";
		FileKiir fk = new FileKiir(path, fileContent);
		fk.stringKiir();
	}

	@Test
	public void existTest() {
		String path = "c:\\test.txt";
		File testFile = new File(path);
		Assert.assertNotNull(testFile.exists());
	}

	@Test
	public void contentTest() {
		String path = "c:\\test.txt";
		String testFile = "";
	    byte[] bytes = null;
		try {
			bytes = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		    testFile = new String (bytes);
		 
		    Assert.assertEquals("test",testFile);
		

	}
	
}
