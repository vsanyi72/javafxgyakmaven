package vs.konyvtarlistazo.konyvtar;


import java.io.File;
import org.junit.Assert;
import org.junit.Test;

public class ListToTextTest {

	@Test
	public void emptyTest() {
		ListToText ltt = init();
		Assert.assertNotNull(ltt.convert());
	}

	public ListToText init() {
		File[] listOfFiles = null;
		String path = "c:\\";
		FileList fileList = new FileList(path);
		listOfFiles = fileList.Read();
		ListToText ltt = new ListToText(listOfFiles, fileList.getAbsolutePath());
		return ltt;
	}

	@Test
	public void rootTest() {
		ListToText ltt = init();
		String directory = ltt.convert();
		Assert.assertEquals(directory.charAt(0), 'c');
		Assert.assertEquals(directory.charAt(1), ':');
		Assert.assertEquals(directory.charAt(2), '\\');
	}
}
