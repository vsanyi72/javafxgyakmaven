package vs.konyvtarlistazo.konyvtar;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

public class AppTest {

	@Test
	public void testFile() {
		String[] args = new String[2];
		args[0] = "c:\\";
		args[1] = "-file";

		String path = "c:\\work\\java02\\konyvtarlistazo.txt";
		File testFile = new File(path);
		testFile.delete();

		App app = new App();
		app.run(args);

		testFile = new File(path);
		Assert.assertNotNull(testFile.exists());
		testFile.delete();
	}

	@Test
	public void testDb() {
		Connection conn;
		int lastId = 0;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dir_nyt", "root", "1234");
			DbKiir dk = new DbKiir(conn);
			dk.write("testText");
			PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM directory");
			ResultSet result = preparedStatement.executeQuery();
			String dir = "";
			while (result.next()) {
				lastId = result.getInt("dir_id");
				dir = result.getString("dir");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] args = new String[2];
		args[0] = "c:\\";
		args[1] = "-db";
		App app = new App();
		app.run(args);

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dir_nyt", "root", "1234");
			PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM directory");
			ResultSet result = preparedStatement.executeQuery();
			String dir = "";
			int dirId = 0;
			while (result.next()) {
				dirId = result.getInt("dir_id");
				dir = result.getString("dir");
			}

			preparedStatement = conn.prepareStatement("DELETE FROM directory WHERE dir_id=?");
			preparedStatement.setInt(1, dirId);
			preparedStatement.executeUpdate();
			preparedStatement = conn.prepareStatement("DELETE FROM directory WHERE dir_id=?");
			preparedStatement.setInt(1, lastId);
			preparedStatement.executeUpdate();

			Assert.assertEquals((lastId + 1), dirId);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
